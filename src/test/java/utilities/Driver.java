package utilities;

import common.Config;
import cucumber.api.Scenario;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class Driver {

    protected static WebDriver driver;
    private static final String BROWSER = Config.getBrowser();
    private final Logger LOGGER = Logger.getLogger(Driver.class.getName());
    public ScenarioData scenarioData =    new ScenarioData();



    public void initialize() {
        if (driver == null)
            createNewDriverInstance();
    }

    /**
     * Creates a new driver instance
     */
    private void createNewDriverInstance() {
        driver = DriverFactory.getDriver(BROWSER);
    }

    /**
     * Returns the current driver
     *
     * @return driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    public void launch(){
        driver.navigate().to("https://www.google.com");
        driver.manage().window().maximize();
    }

    /**
     * Quits the current driver session
     */
    public void destroyDriver() {
        LOGGER.info("destroyDriver started");
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            LOGGER.info("Exception: " + e.getMessage());
        }
        LOGGER.info("destroyDriver completed");
    }

    /**
     * Embeds a screenshot into a scenario
     *
     * @param scenario Scenario status - Passed or Failed
     * @throws IOException General exception caught to allow for graceful failure
     */
    public void embedScreenshot(Scenario scenario) throws IOException {
        LOGGER.info("embedScreenshot started with scenario '" + scenario + "' " + scenario.getStatus());
        byte[] screenshot;
        try {
                screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
        } catch (WebDriverException somePlatformsDontSupportScreenshots) {
            LOGGER.info(somePlatformsDontSupportScreenshots.getMessage());
        }
        LOGGER.info("embedScreenshot completed with scenario '" + scenario + "' " + scenario.getStatus());
    }

    /**
     *  Method reads JSON reports generated in Target folder by Cucumber.
     *
     * @throws Throwable
     */
    @Test
    public void readJsonFiles() throws Throwable {
        File[] files = new File("D:\\schwab_office_files\\Advice Suite Automation\\target\\cucumber-parallel").listFiles();
        String numberOfReports = String.valueOf(files.length);
        scenarioData.setData("numberofreports", numberOfReports);
        for (int i = 0; i < files.length; i++) {
            String componentName;
            System.out.println(files[i].getName());
            Object obj = new JSONParser().parse(new FileReader(files[i].getPath()));
            JSONArray reportJsonArray = (JSONArray) obj;
            JSONObject reportBody = (JSONObject) reportJsonArray.get(0);
            componentName = reportBody.get("name").toString();
            JSONArray elementsArray = (JSONArray) reportBody.get("elements");
            JSONObject elementBody = (JSONObject) elementsArray.get(0);
            String nameOfTheTest = elementBody.get("name").toString();
            scenarioData.setData("name" + i, nameOfTheTest);
            System.out.println("---------------------------------------------------------------------------------------------------------------------------");
            String[] scenarioDetails = nameOfTheTest.split("---");
            System.out.println("Application: "+scenarioDetails[0]);
            System.out.println("Component: "+componentName);
            System.out.println("Scenario: "+scenarioDetails[1]);
            System.out.println("Test ID: "+scenarioDetails[2]);
            JSONArray stepsArray = (JSONArray) elementBody.get("steps");
            long[] duration = new long[stepsArray.size()];
            long totalDuration = 0;
            String testStatus = "unknown";

            //server name fetch
//            JSONObject stepObjectsForServer = (JSONObject) stepsArray.get(0);
//            JSONObject match = (JSONObject) stepObjectsForServer.get("match");
//            JSONArray arguments = (JSONArray) match.get("arguments");
//            JSONObject values = (JSONObject) arguments.get(0);
//            String serverUrl = values.get("val").toString();
//            System.out.println("Server Url: " + serverUrl);
//            scenarioData.setData("server" + i, serverUrl);

            //Duration & Test Status Fetch
            for (int j = 0; j < stepsArray.size(); j++) {
                JSONObject stepObjects = (JSONObject) stepsArray.get(j);
                JSONObject result = (JSONObject) stepObjects.get("result");
                duration[j] = Long.parseLong(result.get("duration").toString());
                testStatus = result.get("status").toString();
                totalDuration = totalDuration + duration[j];
                if (testStatus.equalsIgnoreCase("failed")) {
                    break;
                }
            }

            String steps = "";
            String image = "";
            //Duration & Test Status Fetch
            for (int j = 0; j < stepsArray.size(); j++) {
                JSONObject stepObjects = (JSONObject) stepsArray.get(j);
                String stepDescription =  stepObjects.get("name").toString();
                String keyword =  stepObjects.get("keyword").toString().trim();
                steps = steps + keyword + "----";
                steps = steps + stepDescription + "----";
                JSONObject result = (JSONObject) stepObjects.get("result");
               try {
                   steps = steps + convertNanoSecondsToMinutesAndSeconds(Long.parseLong(result.get("duration").toString())) + "----";
               }catch (NullPointerException n){
                   steps = steps + convertNanoSecondsToMinutesAndSeconds(0) + "----";
               }
                if(j<stepsArray.size()-1) {
                    steps = steps + result.get("status").toString() + "----";
                } else {
                    steps = steps + result.get("status").toString();
                }
                if(j==stepsArray.size()-1) {
                    JSONArray embeddings =  (JSONArray) stepObjects.get("embeddings");
                    JSONObject imageData = (JSONObject) embeddings.get(0);
                    String data =  imageData.get("data").toString();
                    image = data;
                }
            }
            System.out.println("Execution Time: " + convertNanoSecondsToMinutesAndSeconds(totalDuration));
            scenarioData.setData("executiontime" + i, convertNanoSecondsToMinutesAndSeconds(totalDuration));
            System.out.println("Overall Test Status: " + testStatus);
            scenarioData.setData("overallteststatus" + i, testStatus);
            System.out.println("Test steps \n"+steps);
            System.out.println("Image: \n"+image);
            System.out.println("---------------------------------------------------------------------------------------------------------------------------");
            postData(scenarioDetails[0],scenarioDetails[2],scenarioDetails[1],componentName,"12:23:59",convertNanoSecondsToMinutesAndSeconds(totalDuration),testStatus,image,"Maintenance","2020-05-31", StringEscapeUtils.escapeJava(steps));
        }

//        organiseDataAndSaveItToScenarioData();
    }

    /**
     * Method converts nano seconds to --> Min and Secs
     *
     * @param nanoSeconds
     * @return
     */
    public String convertNanoSecondsToMinutesAndSeconds(long nanoSeconds) {
        String minutesAndSeconds;
        long milliseconds = nanoSeconds / 1000000;
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;
        if (minutes > 0) {
            minutesAndSeconds = minutes + " min " + seconds + " sec";
        } else {
            minutesAndSeconds = seconds + " sec";
        }
        return minutesAndSeconds;
    }

    public void postData(String applicationName,String testId,String scenarioName, String componentName, String executionTime,
                                                               String duration, String statusTest, String imageBase, String executionTag, String dateOfExecution, String description)
            throws ClientProtocolException, IOException {


        File file = new File("D:\\schwab_office_files\\Advice Suite Automation\\src\\test\\resources\\external\\body.json");
        String[] content = FileUtils.readFileToString(file, StandardCharsets.UTF_8).split("split");
        int length = content.length;
        String requestBody = content[0]+applicationName+content[1]+testId+content[2]+scenarioName+content[3]+componentName+content[4]+executionTime
                +content[5]+duration+content[6]+statusTest+content[7]+imageBase+content[8]+executionTag+content[9]+dateOfExecution+content[10]+description+content[11];
        RestAssured.baseURI = "http://localhost:8080";
        given().urlEncodingEnabled(true)
                .header("Content-Type", "application/json")
                .body(requestBody)
                .post("/checkAndCreateData")
                .then().statusCode(200);

    }


}
