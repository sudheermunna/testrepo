@all
Feature: Proposal

  @one
  Scenario Outline: AES---Chrome is the best browser---70812
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Mac book Pro---25567
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Base ball and cricket are not similar---79987
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @one
  Scenario Outline: AES---IPL is my favorite league---78890
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Happy Women's Day---73557
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---I like to watch short films---70098
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

