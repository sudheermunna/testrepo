@all
Feature: Performance

  @one
  Scenario Outline: AES---Chrome is the best browser---34212
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Mac book Pro---34556
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Base ball and cricket are not similar---30998
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @one
  Scenario Outline: AES---IPL is my favorite league---38890
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Happy Women's Day---33557
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---I like to watch short films---30089
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @one
  Scenario Outline: AES---Demo next week---34656
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Marvel movies are amazing---32234
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Samsung laptops looks rich---32345
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |
