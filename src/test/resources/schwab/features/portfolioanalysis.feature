@all
Feature: Portfolio Analysis

  @one
  Scenario Outline: AES---Chrome is the best browser---40812
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Mac book Pro---25567
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Base ball and cricket are not similar---49987
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @one
  Scenario Outline: AES---IPL is my favorite league---48890
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Happy Women's Day---43557
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---I like to watch short films---40098
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @one
  Scenario Outline: AES---Demo next week---44656
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Marvel movies are amazing---42234
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Samsung laptops looks rich---42345
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @three
  Scenario Outline: AES---Justice league movies are yet to be released---42945
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @three
  Scenario Outline: AES---Court room dramas are my favorite---24000
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |