@all
Feature: Rebalancer

  @one
  Scenario Outline: AES---Chrome is the best browser---80812
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Mac book Pro---85567
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Base ball and cricket are not similar---89987
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @one
  Scenario Outline: AES---IPL is my favorite league---88890
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |



  @two
  Scenario: AES---Marvel movies are amazing---82234
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Samsung laptops looks rich---82345
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @three
  Scenario Outline: AES---Justice league movies are yet to be released---82945
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @three
  Scenario Outline: AES---Court room dramas are my favorite---80000
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |