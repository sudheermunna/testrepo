@all
Feature: Cash flow

  @one
  Scenario Outline: AES---Create, Update, And Delete Portfolio---42533
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @two
  Scenario: AES---Apple Mobiles are Amazing---42531
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---Ballons are full of Air---42535
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
  Examples:
  | index |
  | 1     |

  @two
  Scenario: AES---Short films are amazing---46758
    When I login to application
    And  I switch to the iFrame at Index "1"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul

  @three
  Scenario Outline: AES---BHP full HD monitors---32889
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @three
  Scenario Outline: AES---Justice league movies are yet to be released---14278
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |

  @three
  Scenario Outline: AES---Court room dramas are my favorite---99999
    When I login to application
    And  I switch to the iFrame at Index "<index>"
    And  I check, analyze
    And  I look for some element
    And  I serach for some ul
    Examples:
      | index |
      | 1     |


